<?php
/**
 * 
 */
namespace App\Libs\DbTable;
use Illuminate\Database\Eloquent\Model;

class menuTable extends Model
{
    protected $table = 'menus';

	const COL_MENU_ID = 'menu_id';

	const COL_MENU_NAME = 'menu_name';

	const COL_MENU_LOCATION = 'menu_location';

	const COL_MENU_STATUS = 'menu_status';

	// public static $instance;

	// public function __construct(){
	// 	parent::__construct();
	// }

	// public static function getInstance(){
	// 	if(is_null(self::$instance)){
	// 		self::$instance = new DbTable_Menu();
	// 	}
	// 	return self::$instance;
	// }

 	
} 
?>