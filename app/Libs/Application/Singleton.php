<?php
/**
 * 
 */
namespace App\Libs\Application;

 class Singleton
 {
 	private static $_instance;
 	
 	final public static function getInstance()
 	{
 		$calledClass = get_called_class();
 		if(!isset(self::$_instance[$calledClass]) || is_null(self::$_instance[$calledClass])){
 			self::$_instance[$calledClass] = new $calledClass();
 		}
 		return self::$_instance[$calledClass];
 	}
 	final protected function __clone(){}
 } 
?>