<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" id="dropdown">
            <div class="row">
                <div class="col-md-12">
                    <div class="row group">
                        <div class="col-md-2 menu">
                             <button class="btn-menu">
                                <span class="fas fa-bars"></span>
                              </button>
                        </div>
                        <div class="col-md-8 logo">
                             <span class="img-logo"></span>
                        </div>
                        <div class="col-md-2 cart">
                             <span class="fas fa-shopping-basket icon-cart"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 consultant">
                    <span class="glyphicon glyphicon-earphone">Tu van mua hang: <span class="phone">0363 07 08 09</span></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 search">
                       <input class="input-search" placeholder="Tim san pham" type="text">
                       <span class="fas fa-search"></span>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12">
             <div class="row" id="banner">
                   <div class="col-sm-3 col-xs-4 logo">
                        <span class="image-logo"></span>
                   </div>
                   <div class="col-sm-6 col-xs-4 search">
                        <div class="container-search">
                           <div class="input-search">
                                <input class="input-search" placeholder="Tim san pham" type="text">
                                <span class="btn-search glyphicon glyphicon-search"></span>
                            </div>
                        </div>
                   </div>
                   <div class="col-sm-3 col-xs-4 group">
                       <div class="container-group">     
                            <span class="fas fa-user user"></span>
                            <span class="fas fa-shopping-cart cart"></span>
                       </div>
                   </div>
             </div>
        </div>
        <nav class="col-xs-12" id="nav">
            <ul class="main-menu">
                <li><a href="#">TRANG CHU</a></li>
                <li class="active">
                    <a href="#">SAN PHAM</a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#">San pham 1</a>
                            <ul class="sub-menu">
                                <li><a href="#">San pham 1.1</a></li>
                                <li>
                                    <a href="#">San pham 1.2</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">San pham 1.2.1</a></li>
                                        <li><a href="#">San pham 1.2.2</a></li>
                                        <li><a href="#">San pham 1.2.3</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Gioi thieu 2.3</a></li>
                            </ul>
                        </li>
                        <li><a href="#">San pham 2</a></li>
                        <li><a href="#">San pham 3</a></li>
                        <li><a href="#">San pham 4</a></li>
                    </ul>
                </li>
                <li><a href="#">BEST SELLER</a></li>
                <li><a href="#">THUONG HIEU</a></li>
                <li><a href="#">KIEN THUC THE THAO</a></li>
                <li><a href="#">TIN TUC</a></li>
            </ul>
        </nav>
    </div>
</div>