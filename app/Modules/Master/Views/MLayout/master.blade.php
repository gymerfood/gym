<!DOCTYPE html>
<html>
<head>
	<title>Gymer Food</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
    <script type="text/javascript" src="<?php echo asset('js/jquery-3.2.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo asset('bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo asset('statics/asset/master/js/main.js'); ?>"></script> 
    <link rel="stylesheet" type="text/css" href="<?php echo asset('bootstrap/css/bootstrap.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo asset('bootstrap/fontawesome/css/all.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo asset('statics/asset/master/sass/main.css'); ?>">  
</head>

<body>
    <div id="main" >
        <div id="header">
            @include('MLayout.header')
        </div>
        <div id="body">
            @yield('content')
        </div>
        <div id="footer">
            @include('MLayout.footer')
        </div>
    </div>
</body>
</html>