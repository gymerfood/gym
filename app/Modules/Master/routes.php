<?php
$namespace = 'App\Modules\Master\Controllers';
Route::group(['module'=>'Master', 'namespace' => $namespace], function() {

    Route::get('/', 'indexController@index');
});