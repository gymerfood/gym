<?php
/**
 * 
 */
namespace App\Modules\Master\Controllers;
use App\Http\Controllers\Controller;

class indexController extends Controller
{	
 	public function index()
 	{   
 		return view('Master::Index.index');
 	}
 	public function test()
 	{
 		return view('Master::Index.test');
 	}
}
?>