<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>admin</title>

    <!-- Bootstrap -->
    <link href="<?php echo asset('vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo asset('vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo asset('vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- iCheck -->
   <link   href="<?php echo asset('vendors/iCheck/skins/flat/green.css');?>" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo asset('vendors/bootstrap-daterangepicker/daterangepicker.css');?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo asset('build/css/custom.min.css');?>" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!--sidebar -->
           @include('ALayout.sidebar')
        <!--sidebar -->
        <!-- top navigation -->
        <div class="top_nav">
            @include('ALayout.header')
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                @yield('content')
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
            @include('ALayout.footer')
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo asset('vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset('vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo asset('vendors/fastclick/lib/fastclick.js'); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo asset('vendors/nprogress/nprogress.js'); ?>"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset('vendors/Chart.js/dist/Chart.min.js'); ?>"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo asset('vendors/jquery-sparkline/dist/jquery.sparkline.min.js'); ?>"></script>
    <!-- Flot -->
    <script src="<?php echo asset('vendors/Flot/jquery.flot.js'); ?>"></script>
    <script src="<?php echo asset('vendors/Flot/jquery.flot.pie.js'); ?>"></script>
    <script src="<?php echo asset('vendors/Flot/jquery.flot.time.js'); ?>"></script>
    <script src="<?php echo asset('vendors/Flot/jquery.flot.stack.js'); ?>"></script>
    <script src="<?php echo asset('vendors/Flot/jquery.flot.resize.js'); ?>"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset('vendors/flot.orderbars/js/jquery.flot.orderBars.js'); ?>"></script>
    <script src="<?php echo asset('vendors/flot-spline/js/jquery.flot.spline.min.js'); ?>"></script>
    <script src="<?php echo asset('vendors/flot.curvedlines/curvedLines.js'); ?>"W></script>
    <!-- DateJS -->
    <script src="<?php echo asset('vendors/DateJS/build/date.js'); ?>"></script>
    <!-- Parsley -->
    <script src="<?php echo asset('vendors/parsleyjs/dist/parsley.min.js'); ?>"></script>
     <!-- iCheck -->
    <script src="<?php echo asset('vendors/iCheck/icheck.min.js'); ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo asset('vendors/moment/min/moment.min.js'); ?>"></script>
    <script src="<?php echo asset('vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="<?php echo asset('build/js/custom.min.js'); ?>"></script>
  </body>
</html>