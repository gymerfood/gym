<?php
$namespaces = 'App\Modules\Admin\Controllers';
Route::group(['module'=>'Admin', 'prefix' => 'admin', 'namespace' => $namespaces], function() {
	   
    Route::get('/', 'indexController@index');
    Route::get('/login', 'loginController@index');


/* Route Menu */
    Route::get('/menu', 'menuController@index');
    Route::get('/menu/edit', 'menuController@edit');
/* /Route Menu */

	Route::get('/get.html', 'menuController@get');
});
?>